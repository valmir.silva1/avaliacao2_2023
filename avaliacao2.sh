#!/bin/bash

while true; do
  clear
  echo "======================================================="
  echo "Bem-vindo ao utilitário de compactação/descompactação!"
  echo "======================================================="
  echo "Escolha uma opção:"
  echo "  1. Trabalhar com arquivo ZIP existente"
  echo "  2. Criar um novo arquivo ZIP"
  echo "  3. Sair"
  echo "======================================================="

  read -p "Digite o número da opção desejada: " option

  case $option in
    1)
      clear
      echo "========================================"
      echo "Arquivos no diretório atual:"
      echo "========================================"
      ls *.zip 2>/dev/null

      read -p "Digite o nome do arquivo ZIP existente ou '.' para voltar: " zip_file

      if [ "$zip_file" == "." ]; then
        continue
      fi

      if [ -f "$zip_file" ]; then
        while true; do
          clear
          echo "==================================================="
          echo "Escolha uma opção para trabalhar com o arquivo ZIP '$zip_file':"
          echo "  1. Listar o conteúdo do arquivo ZIP"
          echo "  2. Pré-visualizar arquivos no arquivo ZIP"
          echo "  3. Adicionar arquivos ao arquivo ZIP"
          echo "  4. Remover arquivos do arquivo ZIP"
          echo "  5. Extrair todo o conteúdo do arquivo ZIP"
          echo "  6. Extrair arquivos específicos do arquivo ZIP"
          echo "  7. Voltar para o menu principal"
          echo "  8. Sair"
	  echo "=================================================="
          read -p "Digite o número da opção desejada: " sub_option

          case $sub_option in
            1)
              unzip -l "$zip_file"
              ;;
            2)
              echo "Arquivos no ZIP '$zip_file':"
              unzip -l "$zip_file"
              
              echo "Escolha um arquivo para pré-visualizar:"
              read -p "Digite o nome do arquivo que deseja pré-visualizar (ou '.' para voltar): " file_to_preview

              if [ "$file_to_preview" == "." ]; then
                continue
              fi

              unzip -p "$zip_file" "$file_to_preview" | less
              ;;
            3)
              echo "Arquivos no diretório atual:"
              ls

              read -p "Digite os nomes dos arquivos a serem adicionados, separados por espaço (ou '.' para voltar): " files_to_add

              if [ "$files_to_add" == "." ]; then
                continue
              fi

              for file_to_add in $files_to_add; do
                if [ -f "$file_to_add" ]; then
                  zip -u "$zip_file" "$file_to_add"
                  echo "Arquivo '$file_to_add' adicionado com sucesso a '$zip_file'."
                else
                  echo "Arquivo '$file_to_add' não encontrado. Ignorando."
                fi
              done
              ;;
            4)
              echo "Arquivos no ZIP '$zip_file':"
              unzip -l "$zip_file"

              read -p "Digite o nome do arquivo a ser removido (ou '.' para voltar): " file_to_remove

              if [ "$file_to_remove" == "." ]; then
                continue
              fi

              zip -d "$zip_file" "$file_to_remove"
              echo "Arquivo removido com sucesso de '$zip_file'."
              ;;
            5)
              clear
              echo "========================================"
              echo "Conteúdo do arquivo ZIP '$zip_file':"
              echo "========================================"
              unzip -l "$zip_file"
              
              read -p "Deseja realmente extrair todo o conteúdo do arquivo ZIP? (S/n): " extract_confirm

              if [ "$extract_confirm" == "S" -o "$extract_confirm" == "s" ]; then
                unzip "$zip_file"
                echo "Conteúdo do arquivo ZIP extraído com sucesso."
              else
                echo "Operação de extração cancelada."
              fi
              ;;
            6)
              echo "Arquivos no ZIP '$zip_file':"
              unzip -l "$zip_file"

              read -p "Digite o nome do arquivo a ser extraído (ou '.' para voltar): " file_to_extract

              if [ "$file_to_extract" == "." ]; then
                continue
              fi

              unzip "$zip_file" "$file_to_extract" -d "${file_to_extract%%.*}"
              echo "Arquivo extraído com sucesso: $file_to_extract"
              ;;
            7)
              break
              ;;
            8)
              echo "Encerrando o script. Adeus!"
              exit
              ;;
            *)
              echo "Opção inválida. Tente novamente."
              ;;
          esac
          read -p "Pressione Enter para continuar..."
        done
      else
        echo "Arquivo ZIP não encontrado. Tente novamente."
      fi
      ;;
    2)
      clear
      read -p "Digite o nome para o novo arquivo ZIP (incluindo a extensão .zip): " new_zip_file
      clear
      echo "========================================"
      echo "Arquivos no diretório atual:"
      echo "========================================"
      ls
      echo "========================================"
      read -p "Digite os nomes dos arquivos a serem adicionados, separados por espaço: " files_to_add
      
      for file_to_add in $files_to_add; do
        if [ -f "$file_to_add" ]; then
          zip "$new_zip_file" "$file_to_add"
          echo "Arquivo '$file_to_add' adicionado com sucesso a '$new_zip_file'."
        else
          echo "Arquivo '$file_to_add' não encontrado. Ignorando."
        fi
      done
      ;;
    3)
      echo "Encerrando o script. Adeus!"
      exit
      ;;
    *)
      echo "Opção inválida. Tente novamente."
      ;;
  esac

  read -p "Pressione Enter para continuar..."
done

